#ifndef DATABASE_H
#define DATABASE_H

#include <QtCore>
#include <QtSql>
#include <list>
#include <QtSql>
#include <QSqlDatabase>
#include <QSqlError>
#include <QtDebug>
#include <QSqlQuery>

struct SListDb
{
    int m_nListId;
    QString m_sListName;
    QString m_sListAddress;
};

class database
{
public:
    database();
    ~database();

    QMap<QString,int> m_SessionMap;
    void CreateUser(QString sLogin, QString sPassword, QString sHashSalt );
    QString GetSalt(QString sLogin);
    bool FindLogin (QString sLogin);
    int GetUserId (QString sLogin, QString sPassword);

    std::list <SListDb> GetList_db (QString id_db, QString sToken);
    std::list<SListDb> Creat_db (QString sName, QString sAddress, QString sToken);
    std::list<SListDb> Update_db(QString sGrafa, QString sNewValue, QString sFindGrafa, QString sFindValue, QString sToken);
    std::list<SListDb> Gelete_db(QString sValue, QString sGrafa, QString sToken);

 };

#endif // DATABASE_H
