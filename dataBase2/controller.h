#ifndef CONTROLLER_H
#define CONTROLLER_H

#include <QtCore>
#include <QtSql>
#include "database.h"

class Controller
{
public:
    Controller(database* pServer);
    ~Controller();
    bool SecondLogin(QString sLogin); // или  сразу  завпрос  в базу?
    void Register_user(QString sLogin, QString sPassword);
    QString Login_on(QString sLogin, QString sPassword);

private:

    database* m_pServer;



};

#endif // CONTROLLER_H
