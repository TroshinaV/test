#include "thread.h"


CThread::CThread(std::string host, int port, QObject *parent)
    : QThread(parent), m_sHost(host), m_iPort(port)
{
    ev_base = event_base_new();
    ev_http = evhttp_new(ev_base);
}

CThread::~CThread()
{
    wait();
    evhttp_free(ev_http);
    event_base_free(ev_base);
}

void CThread::stop()
{
    event_base_loopbreak(ev_base);
}


void CThread::run()
{
    evhttp_bind_socket( ev_http, m_sHost.c_str(), m_iPort);

        evhttp_set_cb(ev_http, "/register", http_register_cb, NULL);
        evhttp_set_cb(ev_http, "/login", http_login_cb, this);
        evhttp_set_cb(ev_http, "/list", http_list_cb, this);
        evhttp_set_cb(ev_http, "/create", http_create_cb, this);
        evhttp_set_cb(ev_http, "/update", http_update_cb, this);
        evhttp_set_cb(ev_http, "/delete", http_delete_cb, this);

    event_base_dispatch (ev_base);
}

void http_register_cb (struct evhttp_request *request, void *ctx)
{

    struct evbuffer *evb;
    struct evkeyvalq user;
    evb = evbuffer_new();

    evhttp_parse_query (request->uri, &user);
    const char* cLogin = evhttp_find_header (&user, "login");
    const char* cPassword = evhttp_find_header(&user, "password");

    if( !cLogin || !cPassword)
        evbuffer_add_printf (evb, "Please enter login and password");

    else
    {
        if(((Controller*)ctx)->SecondLogin(cLogin))
            evbuffer_add_printf (evb, "Please choose another login");
        else
            ((Controller*)ctx)->Register_user(cLogin, cPassword);
            evbuffer_add_printf (evb, "Now please enter");
    }
    evhttp_add_header(request->output_headers, "Content Type", "text/html");
    evhttp_send_reply(request, HTTP_OK, "HTTP_OK", evb);
    evbuffer_free(evb);
    evhttp_clear_headers(&user);
}
void http_login_cb(struct evhttp_request *request, void *ctx)
{
    QString sToken;
    struct evbuffer *evb;
    struct evkeyvalq user;
    evb = evbuffer_new();

    evhttp_parse_query(request->uri, &user);
    const char *cLogin = evhttp_find_header(&user, "login");
    const char *cPassword = evhttp_find_header(&user, "password");

    if(cLogin && cPassword)
    {
        sToken = ((Controller*)ctx)->Login_on(cLogin, cPassword);
        if (sToken.size()>0)
        {
            evbuffer_add_printf(evb, "%s\n", sToken.toStdString().c_str());
            evbuffer_add_printf(evb, "Allowed control database");
        }
        else
            evbuffer_add_printf(evb, "error");
    }
    else
        evbuffer_add_printf(evb, "Enter login and password");

    evhttp_add_header(request->output_headers, "Content Type", "text/html");
    evhttp_send_reply(request, HTTP_OK, "HTTP_OK", evb);
    evhttp_clear_headers(&user);
    evbuffer_free(evb);
}
