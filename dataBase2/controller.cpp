#include "controller.h"
#include <iostream>
#include <stdlib.h>
#include "thread.h"

Controller::Controller(database* pServer)
    :m_pServer(pServer)
{

}

Controller::~Controller()
{
}

void Controller::Register_user(QString sLogin, QString sPassword)
{
    QString sSalt = QString(QString (sLogin) + QString::number(rand() % 123456 +1) + QString (sPassword));
    QString sHashSalt = QCryptographicHash::hash(sSalt.toAscii(), QCryptographicHash::Sha1).toHex();
    QString sPlainPassword = sHashSalt + QString(sPassword) + sHashSalt;
    QString sHashPassword = QCryptographicHash::hash(sPlainPassword.toAscii(), QCryptographicHash::Sha1).toHex();
    m_pServer->CreateUser(sLogin, sHashPassword, sHashSalt);

}
bool Controller::SecondLogin(QString sLogin)
{
    bool bResult = m_pServer->FindLogin(sLogin);
    return bResult;
}
QString Controller::Login_on(QString sLogin, QString sPassword)
{
    QString sToken;
    CThread* pPointer;

    QString sSalt = m_pServer->GetSalt(sLogin);
    QString sPlainPassword = sSalt + QString(sPassword) + sSalt;
    QString sHashPassword = QCryptographicHash::hash(sPlainPassword.toAscii(), QCryptographicHash::Sha1).toHex();

    int nUserId = m_pServer->GetUserId(sLogin, sHashPassword);
    if(nUserId > 0)
    {
      sToken = rand() %123456 + 1;
      sToken = QCryptographicHash::hash(sToken.toAscii(), QCryptographicHash::Sha1).toHex();
      pPointer->m_SessionMap.insert(sToken,nUserId);
    }
    else
        sToken = "error";

   return sToken;
}
