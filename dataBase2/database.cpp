#include "database.h"

database::database()
{
    QTextCodec::setCodecForCStrings(QTextCodec::codecForName("CP1252"));

     QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE", "test");
     db.setDatabaseName("/home/user/test");
     /*db.setHostName( "192.230.1.230");
     db.setUserName("appserver");
     db.setPassword("12345");*/
     if (!db.open()){
         qDebug()<<db.lastError().text();
          return;
     }
     else
         qDebug()<<"ok";
}

database::~database()
{
}

void database::CreateUser(QString sLogin, QString sPassword, QString sHashSalt)
{
    QSqlQuery query(QSqlDatabase::database("test"));

    query.prepare("INSERT INTO uset (login, password, salt) VALUES(:login, :password, :salt)"); // обавляет пользователя в базу
    query.bindValue("login", sLogin);
    query.bindValue("password", sPassword);
    query.bindValue("salt", sHashSalt );
    query.exec();
}

bool database::FindLogin (QString sLogin)
{
    bool bResult = false;
    QSqlQuery query(QSqlDatabase::database("test"));

    query.prepare("SELECT * FROM uset WHERE login = :login");
    query.bindValue("login", sLogin);
    query.exec();
    if(query.next())
        bResult = true;
    else
        bResult = false;

    return bResult;
}

QString database::GetSalt(QString sLogin)
{
    QSqlQuery query(QSqlDatabase::database("test"));
    QString sSalt;
    QString str;
    str = QString("SELECT salt FROM uset WHERE login = \"%1\"").arg(sLogin);
    query.exec(str);

    if (query.next())
      sSalt = query.value(0).toString();
    else
      sSalt = QString();

    return sSalt;
}

int database::GetUserId (QString sLogin, QString sPassword)
{
    QSqlQuery query(QSqlDatabase::database("test"));
    QString str;
    int nUserId;

    str = QString("SELECT id_user FROM uset WHERE login = \"%1\" AND password = \"%2\"").arg(sLogin, sPassword);
    query.exec(str);
    if (query.next())
    {
        int nUserId = query.value(0).toInt();
        return nUserId;
    }
    else
        return -1;
}

std::list <SListDb> database::GetList_db (QString id_db, QString sToken)
{
    std::list <SListDb> Result;

    QSqlQuery query(QSqlDatabase::database("test"));
    query.prepare(QString("SELECT id, name, address FROM klient WHERE id >="+ QString(id_db)));
    query.exec();

    while(query.next())
    {
        SListDb newList;
        newList.m_nListId = query.value(0).toInt();
        newList.m_sListName = query.value(1).toString();
        newList.m_sListAddress = query.value(2).toString();

        Result.push_back(newList);
    }
    return Result;
}

std::list<SListDb> Creat_db(QString sName, QString sAddress, QString sToken)
{
    std::list <SListDb> CreateResult;

    QSqlQuery query(QSqlDatabase::database("test"));
    query.prepare("INSERT INTO klient (name, address)"
                                "VALUES (:name, :address)");
    query.bindValue(":name", sName);
    query.bindValue(":address", sAddress);
    query.exec();
    query.exec("SELECT id, name, address FROM klient WHERE id >=1");

    while(query.next())
    {
      SListDb ListCreate;
      ListCreate.m_nListId = query.value(0).toInt();
      ListCreate.m_sListName = query.value(1).toString();
      ListCreate.m_sListAddress = query.value(2).toString();

      CreateResult.push_back(ListCreate);
    }
    return CreateResult;
}

std::list<SListDb> database::Update_db(QString sGrafa, QString sNewValue, QString sFindGrafa, QString sFindValue, QString sToken)
{
    std::list <SListDb> UpdateResult;
    QSqlQuery query(QSqlDatabase::database("test"));
    QString str;

    str = QString ("UPDATE klient SET %1 = \"%2\" WHERE %3 = \"%4\"").arg(sGrafa, sNewValue, sFindGrafa, sFindValue);
    query.exec(str);
    query.exec("SELECT id, name, address FROM klient WHERE id >=1");

     while(query.next())
        {
          SListDb ListUpdate;
          ListUpdate.m_nListId = query.value(0).toInt();
          ListUpdate.m_sListName = query.value(1).toString();
          ListUpdate.m_sListAddress = query.value(2).toString();

          UpdateResult.push_back(ListUpdate);
        }
     return UpdateResult;
}

std::list<SListDb> database::Gelete_db(QString sValue, QString sGrafa, QString sToken)
{
    std::list <SListDb> DeleteResult;
    QSqlQuery query(QSqlDatabase::database("test"));
    QString str;
    str = QString("DELETE FROM klient WHERE \"%1\" = \"%2\"").arg(sGrafa, sValue);
    query.exec(str);
    query.exec("SELECT id, name, address FROM klient WHERE id >=1");

    while(query.next())
    {
        SListDb ListDelete;
        ListDelete.m_nListId = query.value(0).toInt();
        ListDelete.m_sListName = query.value(1).toString();
        ListDelete.m_sListAddress = query.value(2).toString();

        DeleteResult.push_back(ListDelete);
    }
    return DeleteResult;
}
