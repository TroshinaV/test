#-------------------------------------------------
#
# Project created by QtCreator 2014-10-21T14:35:18
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = testWt
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

INCLUDEPATH += /opt/wt++/include
LIBS += -L/opt/wt++/lib -lwt -lwtdbo -lwtdbosqlite3 -lwthttp -L/opt/boost_1.55/lib -lboost_program_options -lboost_date_time -lboost_locale -lboost_graph -lboost_regex -lboost_filesystem -lboost_signals -lboost_system -lboost_thread -lboost_random

SOURCES += \
    main.cpp \
    thread.cpp \
    database.cpp \
    controller.cpp

HEADERS += \
    controller1.h \
    database1.h \
    thread1.h
