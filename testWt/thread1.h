#ifndef THREAD_H
#define THREAD_H
#include <QCoreApplication>
#include <stdio.h>
#include <event2/event.h>   //libevent
#include <stdlib.h>
#include <evhttp.h>         //libevent
#include <iostream>
#include <sys/stat.h>
#include <fcntl.h>
#include <fstream>
#include <unistd.h>
#include <QtCore>

#include <QtSql>
#include <QSqlDatabase>
#include <QSqlError>
#include <QtDebug>
#include <QSqlQuery>

#include <QThread>

#include <QCryptographicHash>
#include "controller.h"

#ifndef THREAD_H
#define THREAD_H
#include <QCoreApplication>
#include <stdio.h>
#include <event2/event.h>   //libevent
#include <stdlib.h>
#include <evhttp.h>         //libevent
#include <iostream>
#include <sys/stat.h>
#include <fcntl.h>
#include <fstream>
#include <unistd.h>
#include <QtCore>

#include <QtSql>
#include <QSqlDatabase>
#include <QSqlError>
#include <QtDebug>
#include <QSqlQuery>

#include <QThread>

#include <QCryptographicHash>
#include "controller.h"
class CThread : public QThread
{
public:
    CThread(std::string host, int port, Controller *pController);
    ~CThread();



    void stop();

protected:
    void run();

private:
    const std::string m_sHost;
    int m_iPort;

    struct evhttp *ev_http;
    struct event_base *ev_base;
    Controller* m_pAuthController;
};
void http_register_cb (struct evhttp_request *request, void *ctx );
void http_login_cb (struct evhttp_request *request, void *ctx);
void http_list_cb (struct evhttp_request *request, void *ctx);
void http_create_cb (struct evhttp_request *request, void *ctx);
void http_update_cb( struct evhttp_request *request, void *ctx);
void http_url_cb (struct evhttp_request *request, void *ctx);
void http_delete_cb(struct evhttp_request *request, void *ctx);
// void http_hello_cb(struct evhttp_request *request, void *ctx);

#endif // THREAD_H
