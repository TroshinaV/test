#ifndef DATABASE_H
#define DATABASE_H

#include <QtCore>
#include <QtSql>

struct SListDb
{
    int m_nListId;
    QString m_sListName;
    QString m_sListAddress;
};

class database
{
public:
    database();
    ~database();


    QVariant GetList_db(QString id_db);
    QVariant Creat_db(QString sName, QString sAddress);

 };

#endif // DATABASE_H
