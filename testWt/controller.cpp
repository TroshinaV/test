#include "controller1.h"
#include <assert.h>
#include <stdlib.h>
#include "thread1.h"

Controller::Controller(database* pServer)
    :m_pServer(pServer)
{
    qDebug() << "Controller::Controller";
}

Controller::~Controller()
{
}
QVariant Controller::List (QString sId, QString sToken)
{
    if (m_SessionMap.contains(sToken))
        return m_pServer->GetList_db(sId);
    else
    {
        QString str = "Wrong token";
        return QVariant(str);
    }
}
QVariant Controller::Create (QString sName, QString sAddress, QString sToken)
{
    if (m_SessionMap.contains(sToken))
        return m_pServer->Creat_db(sName, sAddress);
    else
    {
        QString str = "Wrong token";
        return QVariant(str);
    }
}
