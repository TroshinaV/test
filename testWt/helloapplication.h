#ifndef HELLOAPPLICATION_H
#define HELLOAPPLICATION_H

#include <Wt/WApplication>
#include <Wt/WBreak>
#include <Wt/WContainerWidget>
#include <Wt/WLineEdit>
#include <Wt/WPushButton>
#include <Wt/WText>

using namespace Wt;

class HelloApplication : public WApplication
{
public:
    WApplication *CreateApplication(const WEnvironment& env);
    HelloApplication(const WEnvironment& env);
    void greet();

private:
    WLineEdit *nameEdit;
    WText *greeting;
};

#endif // HELLOAPPLICATION_H
