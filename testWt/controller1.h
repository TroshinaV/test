#ifndef CONTROLLER_H
#define CONTROLLER_H

#include <QtCore>
#include <QtSql>
#include "database.h"

class Controller
{
public:
    Controller(database* pServer);
    ~Controller();

    QVariant List(QString sId, QString sToken);
    QVariant Create (QString sName, QString sAddress, QString sToken);

private:

    database* m_pServer;
};

#endif // CONTROLLER_H
