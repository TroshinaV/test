#include "helloapplication.h"

WApplication *CreateApplication(const WEnvironment& env)
{
    return new HelloApplication(env);
}


int main(int argc, char **argv)
{
    return WRun (argc,argv, &CreateApplication);
}
