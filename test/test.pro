#-------------------------------------------------
#
# Project created by QtCreator 2014-10-02T09:33:22
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = test
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

INCLUDEPATH += /opt/libevent/include
LIBS += -L/opt/libevent/lib -levent


SOURCES += main.cpp \
    threa.cpp

HEADERS += \
    threa.h
