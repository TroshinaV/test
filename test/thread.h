#ifndef THREAD_H
#define THREAD_H

#include <QCoreApplication>
#include <stdio.h>
#include <event2/event.h>
#include <stdlib.h>
#include <evhttp.h>
#include <iostream>
#include <sys/stat.h>
#include <fcntl.h>
#include <fstream>
#include <unistd.h>
#include <QObject>

#include <QThread>

class CThread : public QThread
{
public:
    CThread( std::string host, int port, QObject *parent = 0);
    ~CThread();


    void stop();

protected:
    void run();

private:
    const std::string m_sHost;
    int m_iPort;

    struct evhttp *ev_http;
    struct event_base *ev_base;
};

void http_url_cb (struct evhttp_request *request, void *ctx);

bool create

#endif // THREAD_H
