#include "thread.h"

CThread::CThread(std::string host, int port, QObject *parent) :
    QThread(parent), m_sHost(host), m_iPort(port)
{
    ev_base = event_base_new();
    ev_http = evhttp_new(ev_base);
}

CThread::~CThread()
{
    wait();
    evhttp_free(ev_http);
    event_base_free(ev_base);
}

void CThread::stop()
{
    event_base_loopbreak(ev_base);
}

void CThread::run()
{
    evhttp_bind_socket( ev_http, m_sHost.c_str(), m_iPort);
    evhttp_set_cb (ev_http, "/url", http_url_cb, NULL);
    event_base_dispatch (ev_base);
}



void http_url_cb (struct evhttp_request *request, void *ctx) //то может быть в ctx?//

{
    struct evbuffer *evb;
    struct evkeyvalq uri_params;
    int file, read_bytes;
    const char *fname;
    struct stat stbuf;

    evb = evbuffer_new();

    std::cout << "Запрос от:" << request->remote_host << ":" << request->remote_port << "URI:" << request->uri << "\n";

   evhttp_parse_query (request->uri, &uri_params);

   fname = evhttp_find_header (&uri_params, "name");

   if(!fname) {                                         // был ли указан параметр name
       evbuffer_add_printf (evb, "Bad request ВОТ ЗДЕСЬ");
       evhttp_send_error (request, 400, "Bad request\n");
       evhttp_clear_headers (&uri_params);
       evbuffer_free(evb);
       return;
   }

   if ((file = open(fname, O_RDONLY)) == -1) {           // Был ли найден указанный файл
       close(file);
       evbuffer_add_printf(evb, "File %s not, found", fname);
       evhttp_send_error(request, 404, "File not found\n");
       evhttp_clear_headers(&uri_params);
       evbuffer_free(evb);
       return;
    }

    fstat (file, &stbuf);
    read_bytes = evbuffer_read(evb, file, stbuf.st_size);
    if (read_bytes == -1){
        close (file);
        evbuffer_add_printf (evb, "Error reading file %s", fname);
        evhttp_send_error(request, 404, "File not found");
        evhttp_clear_headers (&uri_params);
        evbuffer_free (evb);
        return;
    }

    evhttp_add_header (request->output_headers, "Content Type", "text/plain");
    evhttp_send_reply(request, HTTP_OK, "HTTP_OK", evb);

    evhttp_clear_headers(&uri_params);
    evbuffer_free(evb);
    close (file);


}
