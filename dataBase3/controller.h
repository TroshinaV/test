#ifndef CONTROLLER_H
#define CONTROLLER_H

#include <QtCore>
#include <QtSql>
#include "database.h"

class Controller
{
public:
    Controller(database* pServer);
    ~Controller();
    bool SecondLogin(QString sLogin); // или  сразу  завпрос  в базу?
    void Register_user(QString sLogin, QString sPassword);
    QString Login_on(QString sLogin, QString sPassword);

    QVariant List(QString sId, QString sToken);
    QVariant Create (QString sName, QString sAddress, QString sToken);
    QVariant Update (QString sGrafa, QString sNewValue, QString sFinfGrafa, QString sFindValue, QString sToken);
    QVariant Delete (QString sGrafa, QString sValue, QString sToken);
private:

    database* m_pServer;

    QMap< QString, int> m_SessionMap;
};

#endif // CONTROLLER_H
