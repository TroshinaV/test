#include "controller.h"
#include <assert.h>
#include <stdlib.h>
#include "thread.h"

Controller::Controller(database* pServer)
    :m_pServer(pServer)
{
    qDebug() << "Controller::Controller";
}

Controller::~Controller()
{
}

void Controller::Register_user(QString sLogin, QString sPassword)
{
    QString sSalt = QString(QString (sLogin) + QString::number(rand() % 123456 +1) + QString (sPassword));
    QString sHashSalt = QCryptographicHash::hash(sSalt.toUtf8(), QCryptographicHash::Sha1).toHex();
    QString sPlainPassword = sHashSalt + QString(sPassword) + sHashSalt;
    QString sHashPassword = QCryptographicHash::hash(sPlainPassword.toUtf8(), QCryptographicHash::Sha1).toHex();
    m_pServer->CreateUser(sLogin, sHashPassword, sHashSalt);

}
bool Controller::SecondLogin(QString sLogin)
{
    bool bResult = m_pServer->FindLogin(sLogin);
    return bResult;
}
QString Controller::Login_on(QString sLogin, QString sPassword)
{
    QString sToken;
    assert(this);
    QString sSalt = m_pServer->GetSalt(sLogin);
    QString sPlainPassword = sSalt + QString(sPassword) + sSalt;
    QString sHashPassword = QCryptographicHash::hash(sPlainPassword.toUtf8(), QCryptographicHash::Sha1).toHex();

    int nUserId = m_pServer->GetUserId(sLogin, sHashPassword);
    if(nUserId > 0)
    {
        qDebug()<<"nUserId";
        sToken = rand() %123456 + 1;
        sToken = QCryptographicHash::hash(sToken.toUtf8(), QCryptographicHash::Sha1).toHex();
        m_SessionMap.insert(sToken,nUserId);
    }
    else
    {
        qDebug()<<"error (Login_on)";
        sToken = "error";
    }
    return sToken;
}

QVariant Controller::List (QString sId, QString sToken)
{
    if (m_SessionMap.contains(sToken))
        return m_pServer->GetList_db(sId);
    else
    {
        QString str = "Wrong token";
        return QVariant(str);
    }
}
QVariant Controller::Create (QString sName, QString sAddress, QString sToken)
{
    if (m_SessionMap.contains(sToken))
        return m_pServer->Creat_db(sName, sAddress);
    else
    {
        QString str = "Wrong token";
        return QVariant(str);
    }
}

QVariant Controller::Update (QString sGrafa, QString sNewValue, QString sFindGrafa, QString sFindValue, QString sToken)
{
    if(m_SessionMap.contains(sToken))
        return m_pServer->Update_db(sGrafa, sNewValue, sFindGrafa, sFindValue);
    else
    {
        QString str = "Wrong token";
        return QVariant(str);
    }
}
QVariant Controller::Delete (QString sGrafa, QString sValue, QString sToken)
{
    if(m_SessionMap.contains(sToken))
        return m_pServer->Delete_db(sValue, sGrafa);
    else
    {
        QString str = "Wrong token";
        return QVariant(str);
    }
}
