#include "database.h"
#include <QDebug>

database::database()
{
    //QTextCodec::setCodecForCStrings(QTextCodec::codecForName("CP1252"));

     QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE", "test");
     db.setDatabaseName("/home/user/test");
     /*db.setHostName( "192.230.1.230");
     db.setUserName("appserver");
     db.setPassword("12345");*/
     if (!db.open()){
         qDebug()<<db.lastError().text();
          return;
     }
     else
         qDebug()<<"ok";
}

database::~database()
{
}

void database::CreateUser(QString sLogin, QString sPassword, QString sHashSalt)
{
    QSqlQuery query(QSqlDatabase::database("test"));

    query.prepare("INSERT INTO uset (login, password, salt) VALUES(:login, :password, :salt)"); // обавляет пользователя в базу
    query.bindValue("login", sLogin);
    query.bindValue("password", sPassword);
    query.bindValue("salt", sHashSalt );
    query.exec();
    qDebug() << sLogin, sPassword, sHashSalt;
}

bool database::FindLogin (QString sLogin)
{
    bool bResult = false;
    QSqlQuery query(QSqlDatabase::database("test"));

    query.prepare("SELECT * FROM uset WHERE login = :login");
    query.bindValue(":login", sLogin);
    query.exec();
    if(query.next())
        bResult = true;
    else
        bResult = false;

    return bResult;
}

QString database::GetSalt(QString sLogin)
{
    QSqlQuery query(QSqlDatabase::database("test"));
    QString sSalt;
    QString str;
    str = QString("SELECT salt FROM uset WHERE login = \"%1\"").arg(sLogin);
    query.exec(str);

    if (query.next())
      sSalt = query.value(0).toString();
    else
      sSalt = QString();

    return sSalt;
}

int database::GetUserId (QString sLogin, QString sPassword)
{
    QSqlQuery query(QSqlDatabase::database("test"));
    QString str;
    int nUserId;


    str = QString("SELECT id_user FROM uset WHERE login = \"%1\" AND password = \"%2\"").arg(sLogin, sPassword);
    query.exec(str);
    if (query.next())
    {
        int nUserId = query.value(0).toInt();
        return nUserId;
    }
    else
        return -1;

}

QVariant database::GetList_db (QString id_db)
{
    QList< QVariant> Result2;

    QSqlQuery query(QSqlDatabase::database("test"));
    query.prepare("SELECT id, name, address FROM klient WHERE id >=:id");
    query.bindValue(":id", id_db);
    query.exec();

    while(query.next())
    {   QMap<QString, QVariant> Result;
        Result.insert("id", query.value(0));
        Result.insert("name", query.value(1));
        Result.insert("address", query.value(2));

        Result2.push_back(QVariant (Result));
    }
    return QVariant(Result2);
}

QVariant database:: Creat_db(QString sName, QString sAddress)
{
    QList< QVariant> Result2;

    QSqlQuery query(QSqlDatabase::database("test"));
    query.prepare("INSERT INTO klient (name, address)"
                                "VALUES (:name, :address)");
    query.bindValue(":name", sName);
    query.bindValue(":address", sAddress);
    query.exec();
    query.exec("SELECT id, name, address FROM klient WHERE id >=1");

    while(query.next())
    {
      QMap <QString, QVariant> Result;
      Result.insert("id", query.value(0));
      Result.insert("name", query.value(1));
      Result.insert("address", query.value(2));

      Result2.push_back(QVariant (Result));
    }
    return QVariant(Result2);
}

QVariant database::Update_db(QString sGrafa, QString sNewValue, QString sFindGrafa, QString sFindValue)
{
    QSqlQuery query(QSqlDatabase::database("test"));
    QString str;
    QList< QVariant > Result2;

    str = QString ("UPDATE klient SET %1 = \"%2\" WHERE %3 = \"%4\"").arg(sGrafa, sNewValue, sFindGrafa, sFindValue);
    query.exec(str);
    query.exec("SELECT id, name, address FROM klient WHERE id >=1");

     while(query.next())
        {
          QMap <QString, QVariant> Result;
          Result.insert("id", query.value(0));
          Result.insert("name", query.value(1));
          Result.insert("address", query.value(2));

         Result2.push_back(QVariant (Result));
        }
     return QVariant(Result2);
}
QVariant database::Delete_db(QString sValue, QString sGrafa)
{
    QList< QVariant > Result2;
    QSqlQuery query(QSqlDatabase::database("test"));
    QString str;
    str = QString("DELETE FROM klient WHERE \"%1\" = \"%2\"").arg(sGrafa, sValue);
    query.exec(str);
    query.exec("SELECT id, name, address FROM klient WHERE id >=1");

    while(query.next())
    {
        QMap <QString, QVariant> Result;
        Result.insert("id", query.value(0));
        Result.insert("name", query.value(1));
        Result.insert("address", query.value(2));

        Result2.push_back(QVariant (Result));
    }
    return QVariant(Result2);
}
