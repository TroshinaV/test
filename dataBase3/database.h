#ifndef DATABASE_H
#define DATABASE_H

#include <QtCore>
#include <QtSql>

struct SListDb
{
    int m_nListId;
    QString m_sListName;
    QString m_sListAddress;
};

class database
{
public:
    database();
    ~database();

    void CreateUser(QString sLogin, QString sPassword, QString sHashSalt );
    QString GetSalt(QString sLogin);
    bool FindLogin (QString sLogin);
    int GetUserId (QString sLogin, QString sPassword);

    QVariant GetList_db(QString id_db);
    QVariant Creat_db(QString sName, QString sAddress);
    QVariant Update_db(QString sGrafa, QString sNewValue, QString sFindGrafa, QString sFindValue);
    QVariant Delete_db(QString sValue, QString sGrafa);

 };

#endif // DATABASE_H
