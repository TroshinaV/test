#-------------------------------------------------
#
# Project created by QtCreator 2014-10-02T09:33:22
#
#-------------------------------------------------

QT       += core xml

QT       -= gui
QT += sql

TARGET = test
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

INCLUDEPATH += /opt/libevent/include

LIBS += -L/opt/libevent/lib -levent



SOURCES += main.cpp \
    thread.cpp \
    database.cpp \
    controller.cpp \
    json.cpp

HEADERS += \
    thread.h \
    database.h \
    controller.h \
    json.h
