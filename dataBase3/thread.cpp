#include "thread.h"
#include "json.h"
using namespace QtJson;

CThread::CThread(std::string host, int port, Controller *pController)
    : m_sHost(host), m_iPort(port), m_pAuthController(pController)
{
    ev_base = event_base_new();
    ev_http = evhttp_new(ev_base);
}

CThread::~CThread()
{
    wait();
    evhttp_free(ev_http);
    event_base_free(ev_base);
}

void CThread::stop()
{
    event_base_loopbreak(ev_base);
}


void CThread::run()
{
    evhttp_bind_socket( ev_http, m_sHost.c_str(), m_iPort);

        evhttp_set_cb(ev_http, "/register", http_register_cb, m_pAuthController);
        evhttp_set_cb(ev_http, "/login", http_login_cb, m_pAuthController);
        evhttp_set_cb(ev_http, "/list", http_list_cb, m_pAuthController);
        evhttp_set_cb(ev_http, "/create", http_create_cb, m_pAuthController);
        evhttp_set_cb(ev_http, "/update", http_update_cb, m_pAuthController);
        evhttp_set_cb(ev_http, "/delete", http_delete_cb, m_pAuthController);

    event_base_dispatch (ev_base);
}

void http_register_cb (struct evhttp_request *request, void *ctx)
{

    struct evbuffer *evb;
    struct evkeyvalq user;
    evb = evbuffer_new();

    evhttp_parse_query (request->uri, &user);
    const char* cLogin = evhttp_find_header (&user, "login");
    const char* cPassword = evhttp_find_header(&user, "password");

    if( !cLogin || !cPassword)
        evbuffer_add_printf (evb, "Please enter login and password");

    else
    {
        qDebug() << cLogin << cPassword;
        if(((Controller*)ctx)->SecondLogin(cLogin))
            evbuffer_add_printf (evb, "Please choose another login");
        else
        {
            ((Controller*)ctx)->Register_user(cLogin, cPassword);
            evbuffer_add_printf (evb, "Now please enter");
        }
    }
    evhttp_add_header(request->output_headers, "Content Type", "text/html");
    evhttp_send_reply(request, HTTP_OK, "HTTP_OK", evb);
    evbuffer_free(evb);
    evhttp_clear_headers(&user);
}
void http_login_cb(struct evhttp_request *request, void *ctx)
{
    QString sToken;
    struct evbuffer *evb;
    struct evkeyvalq user;
    evb = evbuffer_new();

    evhttp_parse_query(request->uri, &user);
    const char *cLogin = evhttp_find_header(&user, "login");
    const char *cPassword = evhttp_find_header(&user, "password");

    if(cLogin && cPassword)
    {
        sToken = ((Controller*)ctx)->Login_on(cLogin, cPassword);
        qDebug()<<sToken;
        evbuffer_add_printf(evb, "%s\n", sToken.toStdString().c_str());
    }
    else
        evbuffer_add_printf(evb, "Enter login and password");

    evhttp_add_header(request->output_headers, "Content Type", "text/html");
    evhttp_send_reply(request, HTTP_OK, "HTTP_OK", evb);
    evhttp_clear_headers(&user);
    evbuffer_free(evb);
}
void http_list_cb (struct evhttp_request *request, void *ctx)
{
    struct evbuffer *evb;
    struct evkeyvalq id;
    QVariant sList;

    evb = evbuffer_new();
    evhttp_parse_query (request->uri, &id);
    const char *cIdList = evhttp_find_header (&id, "id");
    const char *cToken = evhttp_find_header (&id, "token");
    if(cToken)
      sList = ((Controller*)ctx)->List(cIdList, cToken);
    else
      sList = "Enter token";

    //!!!
    sList = QtJson::serialize(sList);

    evbuffer_add_printf(evb, sList.toByteArray().constData());
    evhttp_add_header(request->output_headers, "Content Type", "text/html");
    evhttp_send_reply(request, HTTP_OK, "HTTP_OK", evb);
    evhttp_clear_headers(&id);
    evbuffer_free(evb);
}
void http_create_cb (struct evhttp_request *request, void *ctx)
{
    struct evbuffer *evb;
    struct evkeyvalq parametr;
    QVariant sList;

    evb = evbuffer_new();
    evhttp_parse_query (request->uri, &parametr);
    const char *cAddName = evhttp_find_header (&parametr, "name");
    const char *cAddAddress = evhttp_find_header(&parametr, "address");
    const char *cToken = evhttp_find_header(&parametr, "token");
    if(cToken)
      sList = ((Controller*)ctx)->Create(cAddName, cAddAddress, cToken);
    else
      sList = "Enter token";

    sList = QtJson::serialize(sList);
    evbuffer_add_printf(evb, sList.toByteArray().constData());
    evhttp_add_header(request->output_headers, "Content Type", "text/html");
    evhttp_send_reply(request, HTTP_OK, "HTTP_OK", evb);
    evhttp_clear_headers(&parametr);
    evbuffer_free(evb);
}
void http_update_cb( struct evhttp_request *request, void *ctx)
{
    struct evbuffer *evb;
    struct evkeyvalq parametr;
    QVariant sList;

    evb = evbuffer_new();
    evhttp_parse_query(request->uri, &parametr);
    const char *cGrafa = evhttp_find_header(&parametr, "grafa");
    const char *cNewValue = evhttp_find_header(&parametr, "newValue");
    const char *cFindGrafa = evhttp_find_header(&parametr, "findGrafa");
    const char *cFindValue = evhttp_find_header(&parametr,"findValue");
    const char *cToken = evhttp_find_header(&parametr, "token");

    if(cToken)
      sList = ((Controller*)ctx)->Update(cGrafa, cNewValue, cFindGrafa, cFindValue, cToken);
    else
    {
      sList = "Enter token";
      qDebug()<< sList;
    }
    sList=QtJson::serialize(sList);

    evbuffer_add_printf(evb, sList.toByteArray().constData());
    evhttp_add_header(request->output_headers, "Content Type", "text/html");
    evhttp_send_reply(request, HTTP_OK, "HTTP_OK", evb);
    evbuffer_free(evb);
    evhttp_clear_headers(&parametr);
}
void http_delete_cb (struct evhttp_request *request, void *ctx)
{
    struct evbuffer *evb;
    struct evkeyvalq parametr;
    QVariant sList;

    evb = evbuffer_new();
    evhttp_parse_query(request->uri, &parametr);
    const char *cValue = evhttp_find_header(&parametr, "value");
    const char *cGrafa = evhttp_find_header(&parametr, "grafa");
    const char *cToken = evhttp_find_header(&parametr, "token");

    if(cToken)
      sList = ((Controller*)ctx)->Delete(cGrafa, cValue, cToken);
    else
    {
      sList = "Enter token";
      qDebug()<< sList;
    }

    sList=QtJson::serialize(sList);
    evbuffer_add_printf(evb, sList.toByteArray().constData());
    evhttp_add_header(request->output_headers, "Content Type", "text/html");
    evhttp_send_reply(request, HTTP_OK, "HTTP_OK", evb);
    evbuffer_free(evb);
    evhttp_clear_headers(&parametr);
}
