#include "thread.h"



CThread::CThread(std::string host, int port, QObject *parent)
    : QThread(parent), m_sHost(host), m_iPort(port)
{
    ev_base = event_base_new();
    ev_http = evhttp_new(ev_base);
}

CThread::~CThread()
{
    wait();
    evhttp_free(ev_http);
    event_base_free(ev_base);
}

void CThread::stop()
{
    event_base_loopbreak(ev_base);
}

void CThread::run()
{
    evhttp_bind_socket( ev_http, m_sHost.c_str(), m_iPort);

        evhttp_set_cb(ev_http, "/login", http_login_cb, this);
         evhttp_set_cb(ev_http, "/register", http_register_cb, NULL);
        evhttp_set_cb(ev_http, "/list", http_list_cb, this);
        evhttp_set_cb(ev_http, "/create", http_create_cb, this);
        evhttp_set_cb(ev_http, "/update", http_update_cb, this);
        evhttp_set_cb(ev_http, "/delete", http_delete_cb, this);
      evhttp_set_cb(ev_http, "/hello", http_hello_cb, NULL);



    event_base_dispatch (ev_base);
}

void http_hello_cb(struct evhttp_request *request, void *ctx)
{
    struct evbuffer *evb;
    evb = evbuffer_new();
    evbuffer_add_printf(evb, "Soedinenie s serverom udalos\n");
    evhttp_send_reply(request, HTTP_OK, "HTTP_OK", evb);
    evbuffer_free(evb);

}

void http_register_cb (struct evhttp_request *request, void *ctx)
{
    QSqlQuery query(QSqlDatabase::database("test"));
    struct evbuffer *evb;
    struct evkeyvalq user;


    evb = evbuffer_new();

    evhttp_parse_query (request->uri, &user);
    const char* cLogin = evhttp_find_header (&user, "login");
    const char* cPassword = evhttp_find_header(&user, "password");

    if( !cLogin || !cPassword)
    {
        evbuffer_add_printf (evb, "Please enter login and password");
        evhttp_send_reply(request, HTTP_BADREQUEST, "400", evb);
        evhttp_clear_headers (&user);
        evbuffer_free (evb);
        return ;
    }
    else
    {
        query.prepare("SELECT * FROM uset WHERE login = :login");
        query.bindValue("login", cLogin);
        query.exec();
        if(query.next())
            evbuffer_add_printf(evb, "Please choose another login");
        else
        {
        QString sSalt = QString(QString (cLogin) + QString::number(rand() % 123456 +1) + QString (cPassword));
        QString sHashSalt = QCryptographicHash::hash(sSalt.toAscii(), QCryptographicHash::Sha1).toHex();

        QString sPlainPassword = sHashSalt + QString(cPassword) + sHashSalt;
        QString sHashPassword = QCryptographicHash::hash(sPlainPassword.toAscii(), QCryptographicHash::Sha1).toHex();

        query.prepare("INSERT INTO uset (login, password, salt) VALUES(:login, :password, :salt)"); // обавляет пользователя в базу
        query.bindValue("login", cLogin);
        query.bindValue("password", sHashPassword);
        query.bindValue("salt", sHashSalt );
        query.exec();
        evbuffer_add_printf(evb, "Now please enter");
        }
        evhttp_add_header(request->output_headers, "Content Type", "text/html");
        evhttp_send_reply(request, HTTP_OK, "HTTP_OK", evb);
    }

        evbuffer_free(evb);
        evhttp_clear_headers(&user);

}

void http_login_cb(struct evhttp_request *request, void *ctx)
{
    QSqlQuery query(QSqlDatabase::database("test"));
    struct evbuffer *evb;
    struct evkeyvalq user;
    QString sToken;

    CThread* pPointer = (CThread*)ctx;

    evb = evbuffer_new();
    evhttp_parse_query(request->uri, &user);
    const char *cLogin = evhttp_find_header(&user, "login");
    const char *cPassword = evhttp_find_header(&user, "password");

    // получение соли
    if (cLogin && cPassword)
    {
       QString str;
       QString sSalt;
       int nUserId=0;

       str = QString("SELECT salt FROM uset WHERE login = \"%1\"").arg(cLogin);
       query.exec(str);

       if (query.next())
         sSalt = query.value(0).toString();
       else
           sSalt = QString();

       QString sPlainPassword = sSalt + QString(cPassword) + sSalt;
       QString sHashPassword = QCryptographicHash::hash(sPlainPassword.toAscii(), QCryptographicHash::Sha1).toHex();

       str = QString("SELECT id_user FROM uset WHERE login = \"%1\" AND password = \"%2\"").arg(cLogin, sHashPassword);

       query.exec(str);
       if (query.next())
           nUserId = query.value(0).toInt();
       else
           return ;

       if(nUserId > 0)
       {
         sToken = rand() %123456 + 1;
         sToken = QCryptographicHash::hash( sToken.toAscii(), QCryptographicHash::Sha1).toHex();
         pPointer->m_SessionMap.insert(sToken,nUserId);

         evbuffer_add_printf(evb, "%s\n", sToken.toStdString().c_str());
         evbuffer_add_printf(evb, "Allowed control database");
        }
         else
       {
         sToken ="error";
         evbuffer_add_printf(evb, "error");
       }
    }
    else
    evbuffer_add_printf(evb, "Enter login and password");



    evhttp_add_header(request->output_headers, "Content Type", "text/html");
    evhttp_send_reply(request, HTTP_OK, "HTTP_OK", evb);
    evhttp_clear_headers(&user);
    evbuffer_free(evb);


}

void http_list_cb (struct evhttp_request *request, void *ctx)
{
    struct evbuffer *evb;
    struct evkeyvalq id;


    evb = evbuffer_new();
    CThread* pPointer = (CThread*)ctx;

    evhttp_parse_query (request->uri, &id);
    const char *cIdList = evhttp_find_header (&id, "id");
    const char *cToken = evhttp_find_header (&id, "token");
     if(cToken)
     {
         if(pPointer->m_SessionMap.contains(cToken))
         {
             QSqlQuery query(QSqlDatabase::database("test"));
             qDebug()<<"Zapros";
             query.exec(QString("SELECT id, name, address FROM klient WHERE id >="+ QString(cIdList)));
             if(!query.isActive())
             {
                 qDebug() << "Zapros ne udalsya";
             }
             else
             {
                  qDebug() << "Zapros udalsya";

                  while(query.next())
                  {
                      qDebug()<<query.value(0).toString()<<query.value(1).toString()<<query.value(2).toString();
                      evbuffer_add_printf (evb, "%s,%s,%s\n", query.value(0).toByteArray().constData(), query.value(1).toByteArray().constData(), query.value(2).toByteArray().constData() );
                  }
             }
         }
         else
         {
             evbuffer_add_printf(evb, "Wrong token");
             qDebug() << "Wrong token";
         }
     }
    else
     {
         evbuffer_add_printf(evb, "Enter token");
         qDebug() << "Enter token";

     }

    evhttp_add_header (request->output_headers, "Content Type", "text/html");
    evhttp_send_reply(request, HTTP_OK, "HTTP_OK", evb);
    evhttp_clear_headers(&id);
    evbuffer_free(evb);


}

void http_create_cb (struct evhttp_request *request, void *ctx)
{
    struct evbuffer *evb;
    struct evkeyvalq parametr;

    evb = evbuffer_new();
    CThread* pPointer = (CThread*)ctx;

    evhttp_parse_query (request->uri, &parametr);
    const char *cAddName = evhttp_find_header (&parametr, "name");
    const char *cAddAddress = evhttp_find_header(&parametr, "address");
    const char *cToken = evhttp_find_header(&parametr, "token");

    if(cToken)
    {
        if(pPointer->m_SessionMap.contains(cToken))
        {
            QSqlQuery query(QSqlDatabase::database("test"));

             query.prepare("INSERT INTO klient (name, address)"
                            "VALUES (:name, :address)");
             query.bindValue(":name", cAddName);
             query.bindValue(":address", cAddAddress);
             query.exec();
             query.exec("SELECT id, name, address FROM klient WHERE id >=1");

             while(query.next())
             {
                qDebug()<<query.value(0).toString()<<query.value(1).toString()<<query.value(2).toString();
                evbuffer_add_printf (evb, "%s,%s,%s\n", query.value(0).toByteArray().constData(), query.value(1).toByteArray().constData(), query.value(2).toByteArray().constData() );
             }
        }
        else
        {
            evbuffer_add_printf(evb, "Wrong token");
            qDebug() << "Wrong token";
        }
    }
   else
    {
        evbuffer_add_printf(evb, "Enter token");
        qDebug() << "Enter token";
    }
    evhttp_add_header(request->output_headers, "Content Type", "text/html");
    evhttp_send_reply(request, HTTP_OK, "HTTP_OK", evb);
    evhttp_clear_headers(&parametr);
    evbuffer_free(evb);
}

void http_update_cb( struct evhttp_request *request, void *ctx)
{

    struct evbuffer *evb;
    struct evkeyvalq parametr;

    evb = evbuffer_new();
    CThread *pPointer = (CThread*)ctx;

    evhttp_parse_query(request->uri, &parametr);
    const char *cGrafa = evhttp_find_header(&parametr, "grafa");
    const char *cNewValue = evhttp_find_header(&parametr, "newValue");
    const char *cFindGrafa = evhttp_find_header(&parametr, "findGrafa");
    const char *cFindValue = evhttp_find_header(&parametr,"findValue");
    const char *cToken = evhttp_find_header(&parametr, "token");

    if(cToken)
    {
        if(pPointer->m_SessionMap.contains(cToken))
        {
            QSqlQuery query(QSqlDatabase::database("test"));

            QString str;
            str = QString ("UPDATE klient SET %1 = \"%2\" WHERE %3 = \"%4\"").arg(cGrafa, cNewValue, cFindGrafa, cFindValue);
            qDebug()<<str;
            query.exec(str);

            query.exec("SELECT id, name, address FROM klient WHERE id >=1");

             while(query.next())
                {
                  qDebug()<<query.value(0).toString()<<query.value(1).toString()<<query.value(2).toString();
                  evbuffer_add_printf(evb, "%s,%s,%s\n", query.value(0).toByteArray().constData(), query.value(1).toByteArray().constData(), query.value(2).toByteArray().constData());
                }
        }
        else
        {
            evbuffer_add_printf(evb, "Wrong token");
            qDebug() << "Wrong token";
        }
    }
   else
    {
        evbuffer_add_printf(evb, "Enter token");
        qDebug() << "Enter token";
    }

     evhttp_add_header(request->output_headers, "Content Type", "text/html");
     evhttp_send_reply(request, HTTP_OK, "HTTP_OK", evb);
     evbuffer_free(evb);
     evhttp_clear_headers(&parametr);

 }

void http_delete_cb (struct evhttp_request *request, void *ctx)
{
    struct evbuffer *evb;
    struct evkeyvalq parametr;

    evb = evbuffer_new();
    CThread *pPointer = (CThread*)ctx;

    evhttp_parse_query(request->uri, &parametr);

    const char *cValue = evhttp_find_header(&parametr, "value");
    const char *cGrafa = evhttp_find_header(&parametr, "grafa");
    const char *cToken = evhttp_find_header(&parametr, "token");


    if(cToken)
    {
        if(pPointer->m_SessionMap.contains(cToken))
        {
            QSqlQuery query(QSqlDatabase::database("test"));

            QString str;
            str = QString("DELETE FROM klient WHERE \"%1\" = \"%2\"").arg(cGrafa, cValue);
            qDebug()<< str;
            query.exec(str);

            query.exec("SELECT id, name, address FROM klient WHERE id >=1");

            while(query.next())
            {
                qDebug()<<query.value(0).toString() << query.value(1).toString() << query.value(2).toString();
                evbuffer_add_printf(evb, "%s,%s,%s", query.value(0).toByteArray().constData(), query.value(1).toByteArray().constData(), query.value(2).toByteArray().constData());
            }
        }
        else
        {
            evbuffer_add_printf(evb, "Wrong token");
            qDebug() << "Wrong token";
        }
    }
   else
    {
        evbuffer_add_printf(evb, "Enter token");
        qDebug() << "Enter token";
    }

    evhttp_add_header(request->output_headers, "Content Type", "text/html");
    evhttp_send_reply(request, HTTP_OK, "HTTP_OK", evb);
    evbuffer_free(evb);
    evhttp_clear_headers(&parametr);
}

void http_url_cb (struct evhttp_request *request, void *ctx)
{
    struct evbuffer *evb;
    struct evkeyvalq uri_params;
    int file, read_bytes;
    const char *fname;
    struct stat stbuf;

    evb = evbuffer_new();

    std::cout << "Запрос от:" << request->remote_host << ":" << request->remote_port << "URI:" << request->uri << "\n";

   evhttp_parse_query (request->uri, &uri_params);

   fname = evhttp_find_header (&uri_params, "name");

   if(!fname) {                                         // был ли указан параметр name
       evbuffer_add_printf (evb, "Bad request");
       evhttp_send_error (request, 400, "Bad request\n");
       evhttp_clear_headers (&uri_params);
       evbuffer_free(evb);
       return;
   }

   if ((file = open(fname, O_RDONLY)) == -1) {           // Был ли найден указанный файл
       close(file);
       evbuffer_add_printf(evb, "File %s not, found", fname);
       evhttp_send_error(request, 404, "File not found\n");
       evhttp_clear_headers(&uri_params);
       evbuffer_free(evb);
       return;
    }

    fstat (file, &stbuf);
    read_bytes = evbuffer_read(evb, file, stbuf.st_size);
    if (read_bytes == -1){
        close (file);
        evbuffer_add_printf (evb, "Error reading file %s", fname);
        evhttp_send_error(request, 404, "File not found");
        evhttp_clear_headers (&uri_params);
        evbuffer_free (evb);
        return;
    }

    evhttp_add_header (request->output_headers, "Content Type", "text/plain");
    evhttp_send_reply(request, HTTP_OK, "HTTP_OK", evb);

    evhttp_clear_headers(&uri_params);
    evbuffer_free(evb);
    close (file);


}
