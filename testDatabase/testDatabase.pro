#-------------------------------------------------
#
# Project created by QtCreator 2014-10-02T09:33:22
#
#-------------------------------------------------

QT       += core

QT       -= gui
QT += sql

TARGET = test
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

INCLUDEPATH += /opt/libevent/include

LIBS += -L/opt/libevent/lib -levent



SOURCES += main.cpp \
    thread.cpp

HEADERS += \
    thread.h
